# Keil.STM32F1xx_DFP 2.4.0 资源文件

## 简介

本仓库提供 Keil.STM32F1xx_DFP 2.4.0 版本的资源文件下载。该资源文件包含了针对 STM32F1xx 系列微控制器的设备支持包（Device Family Pack），适用于 Keil MDK 开发环境。

## 版本信息

- **版本号**: 2.4.0
- **发布日期**: 2021-12-10

## 更新内容

- **标准外设库更新**: 更新至 V3.6.0 版本。
- **CMSIS-Driver 更新**: 更新免责声明。
- **CAN 模块修正**:
  - 修正 `SetBitrate` 函数，确保 Silent 和 Loopback 模式保持不变。
  - 修正 `SetMode` 函数，确保在激活 NORMAL 模式时清除 Silent 和 Loopback 模式。
- **EMAC DMA 支持**: 增加对 ARM Compiler 6 的支持。
- **MCI I2C 更新**: 将空延迟循环替换为 `_NOP()`。
- **I2C 更新**: 更新 `I2C_GetDataCount` 函数，当 Slave 不是时返回 -1。

## 使用说明

1. **下载资源文件**: 点击仓库中的下载链接，获取 `Keil.STM32F1xx_DFP.2.4.0.pack` 文件。
2. **安装到 Keil MDK**:
   - 打开 Keil MDK 开发环境。
   - 导航到 `Pack Installer` 工具。
   - 选择 `File` -> `Import`，然后选择下载的 `.pack` 文件进行安装。
3. **验证安装**:
   - 安装完成后，在 `Pack Installer` 中搜索 `STM32F1xx`，确保 `Keil.STM32F1xx_DFP` 版本显示为 2.4.0。

## 注意事项

- 请确保您的 Keil MDK 版本与该资源文件兼容。
- 在安装新版本之前，建议备份现有项目和配置。

## 许可证

本资源文件遵循相应的开源许可证，具体信息请参考文件中的许可证声明。

## 贡献

欢迎提交问题和改进建议。如果您有任何疑问或需要帮助，请在仓库中创建一个 Issue。

## 联系我们

如有任何问题或合作意向，请联系 [您的联系方式]。

---

感谢您使用 Keil.STM32F1xx_DFP 2.4.0 资源文件！